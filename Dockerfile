#FROM java:openjdk-8-alpine
#FROM openjdk:14-jdk-alpine
#FROM openjdk:latest
#FROM java:openjdk-8-jre-alpine
FROM public.ecr.aws/bitnami/java:1.8
#WORKDIR /usr/src/app
WORKDIR /
COPY ./target/spring-petclinic-2.2.0.BUILD-SNAPSHOT.jar ./app.jar
#RUN mkdir -p /tmp
RUN ls -ltra
EXPOSE 8080
CMD java -jar app.jar
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/urandom","-jar","./app.jar", "--port=8181","-debug=true"]